/**
 * Invisible Connections.
 * draw some meaningless shapes with Fibonacci number.
 *
 * Processing 3.5.3
 * @author @deconbatch
 * @version 0.1
 * created 0.1 2020.03.29
 */

void setup() {

  size(980, 980);
  colorMode(HSB, 360, 100, 100, 100);
  rectMode(CENTER);
  textAlign(CENTER, CENTER);
  smooth();

  textFont(createFont("FreeSerif Bold Italic",12,true));

}

void draw() {

  int frmMax = 3;  // draw three images
  int seqMax = 28;
  int fibMin = 0;
  int fibDiv = 1;
  int fibCnt = fibMin;
  float baseHue = random(360.0);
  float multRad = 1.0 / floor(random(1.0, 8.0));
  ArrayList<Integer> fib = new ArrayList<Integer>();

  // Fibonacci number
  for (int seqCnt = 0; seqCnt < seqMax; seqCnt++) {
    fib.add(fibCnt);
    int fibLast = fibCnt;
    fibCnt += fibDiv;
    fibDiv = fibLast;
  }
  
  translate(width * 0.5, height * 0.5);
  for (int frmCnt = 0; frmCnt < frmMax; frmCnt++) {

    // Fibonacci number -> rotational coordinates
    ArrayList<PVector> pvs = new ArrayList<PVector>();
    for (int seqCnt = 0; seqCnt < seqMax; seqCnt++) {
      float eRadian = (PI * fib.get(seqCnt) * multRad) % TWO_PI;
      float eRadius = width * (0.5 - seqCnt * 0.5 / seqMax);
      float eX = eRadius * cos(eRadian);
      float eY = eRadius * sin(eRadian);
      pvs.add(new PVector(eX, eY));
    }

    // draw ellipses from big to small
    background(0.0, 0.0, 0.0, 100.0);
    for (int seqCnt = seqMax - 1; seqCnt >= 0; seqCnt--) {
      float eSiz = fib.get(seqCnt) * 0.01;
      float eHue = (baseHue + (fib.get(seqCnt) * 0.001) % 60.0) % 360.0;
      
      strokeWeight(9.0);
      stroke(0.0, 0.0, 90.0, 100.0);
      fill(eHue, 40.0, 80.0, 100.0);
      ellipse(pvs.get(seqCnt).x, pvs.get(seqCnt).y, eSiz, eSiz);

      strokeWeight(3.0);
      stroke(eHue, 80.0, 80.0, 100.0);
      noFill();
      ellipse(pvs.get(seqCnt).x, pvs.get(seqCnt).y, eSiz, eSiz);
    }

    // draw lines
    noFill();
    for (int seqCnt = seqMax - 1; seqCnt > 0; seqCnt--) {
      float fX = pvs.get(seqCnt).x;
      float fY = pvs.get(seqCnt).y;
      for (int toCnt = seqCnt - 1; toCnt >= 0; toCnt--) {
        float tX = pvs.get(toCnt).x;
        float tY = pvs.get(toCnt).y;
        float distance = dist(fX, fY, tX, tY);
        if (distance > width * 0.1 && distance < width * 0.2) {
          strokeWeight(1.0);
          stroke(0.0, 0.0, 90.0, 100.0);
          line(fX, fY, tX, tY);

          strokeWeight(1.0);
          stroke(baseHue % 360.0, 80.0, 80.0, 100.0);
          dashedLine(fX, fY, tX, tY);
        }
      }
    }

    // draw number
    noStroke();
    for (int seqCnt = seqMax - 1; seqCnt >= 0; seqCnt--) {
      fill(0.0, 0.0, 90.0, 100.0);
      ellipse(pvs.get(seqCnt).x, pvs.get(seqCnt).y, 20.0, 20.0);
      
      fill(baseHue % 360.0, 80.0, 30.0, 100.0);
      text(seqCnt, pvs.get(seqCnt).x, pvs.get(seqCnt).y);
    }

    casing(baseHue);
    saveFrame("frames/" + String.format("%04d", frmCnt + 1) + ".png");

    // draw another shape with each image
    baseHue += 120.0;
    if (multRad == 1.0) {
      multRad = 1.0 / floor(random(3.0, 8.0));
    } else {
      multRad *= 2.0;
    }
  }
  exit();

}

/**
 * dashedLine : draw dashed line
 * @param  _fX, _fY : line start coordinates.
 * @param  _tX, _tY : line end coordinates.
 */
private void dashedLine(float _fX, float _fY, float _tX, float _tY) {
  float step = 5.0;
  float distance = dist(_fX, _fY, _tX, _tY);
  float radian = atan((_tY - _fY) / (_tX - _fX));
  int   dashMax = floor(distance/ step);
  
  if ((_tX - _fX) < 0.0) {
    radian += PI;
  }

  float fromX = _fX;
  float fromY = _fY;
  for (int dashCnt = 0; dashCnt < dashMax; dashCnt++) {
    float toX = fromX + step * cos(radian);
    float toY = fromY + step * sin(radian);
    if (dashCnt % 3 == 0) {
      line(fromX, fromY, toX, toY);
    }
    fromX = toX;
    fromY = toY;
  }
}

/**
 * casing : draw fancy casing
 * @param  _baseHue : casing color.
 */
private void casing(float _baseHue) {
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(50.0);
  stroke(_baseHue % 360.0, 40.0, 30.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(40.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();
}
